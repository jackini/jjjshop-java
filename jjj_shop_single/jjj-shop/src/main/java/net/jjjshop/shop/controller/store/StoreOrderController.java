package net.jjjshop.shop.controller.store;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.jjjshop.framework.common.api.ApiResult;
import net.jjjshop.framework.core.pagination.Paging;
import net.jjjshop.framework.log.annotation.OperationLog;
import net.jjjshop.shop.param.store.StoreOrderPageParam;
import net.jjjshop.shop.service.store.StoreOrderService;
import net.jjjshop.shop.vo.store.StoreOrderVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(value = "index", tags = {"index"})
@RestController
@RequestMapping("/shop/store/order")
public class StoreOrderController {

    @Autowired
    private StoreOrderService storeOrderService;

    @RequestMapping(value = "/index", method = RequestMethod.POST)
    @RequiresPermissions("/store/order/index")
    @OperationLog(name = "index")
    @ApiOperation(value = "index", response = String.class)
    public ApiResult<Paging<StoreOrderVo>> index(@Validated @RequestBody StoreOrderPageParam storeOrderPageParam) throws Exception {
        return ApiResult.ok(storeOrderService.getList(storeOrderPageParam));
    }
}
